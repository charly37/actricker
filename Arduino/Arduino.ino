//https://github.com/bblanchon/ArduinoJson
#include <ArduinoJson.h>

//http://playground.arduino.cc/Main/DHTLib

#include <dht.h>

/////////////////////
// DHT library
/////////////////////
#define DHT22_PIN A1
dht DHT;

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

class acTricker
{
  public:
    void goToColor();
    void setRandome();
    void update();
    acTricker(int iSmallFanPin, int iBigFanPin, int iPelltierPin);
    void printDebug();
    void setup();

    int _mosfet_big_fan_pin;
    int _mosfet_small_fan_pin;
    int _mosfet_pelltier_pin;

    int _small_fan_speed;
    int _big_fan_speed;
    int _pelltier_is_on;

    //Time between each delta color change when the process is drifting from one color to another
    const int _driftSpeed = 5000;

    //time of the last color drift reach
    unsigned long _lastColorReach;
};

acTricker::acTricker(int iSmallFanPin, int iBigFanPin, int iPelltierPin)
{
  this->_mosfet_big_fan_pin = iBigFanPin;
  this->_mosfet_small_fan_pin = iSmallFanPin;
  this->_mosfet_pelltier_pin = iPelltierPin;

  //_lastColorReach = millis();
}

void acTricker::setup()
{
  pinMode(this->_mosfet_big_fan_pin, OUTPUT);
  pinMode(this->_mosfet_small_fan_pin, OUTPUT);
  pinMode(this->_mosfet_pelltier_pin, OUTPUT);


  _small_fan_speed = 0;
  _big_fan_speed = 0;
  _pelltier_is_on = 0;
  this->update();

}


void acTricker::printDebug()
{
  Serial.print("_small_fan_speed:");
  Serial.print(this->_small_fan_speed);
  Serial.print("_big_fan_speed:");
  Serial.print(this->_big_fan_speed);
  Serial.print("_pelltier_is_on:");
  Serial.print(this->_pelltier_is_on);
  Serial.println("");
}


void acTricker::update()
{
  Serial.println("Updating device with:");
  this->printDebug();
  analogWrite(this->_mosfet_big_fan_pin, this->_big_fan_speed);
  analogWrite(this->_mosfet_small_fan_pin, this->_small_fan_speed);
  analogWrite(this->_mosfet_pelltier_pin, this->_pelltier_is_on);
}



///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

//Create the light object with pin 6,9,10.
acTricker _tricker(10, 6, 9);

void setup() {
  //USB Serial port.
  Serial.begin(115200);

  //BT module serial port.
  Serial1.begin(115200);

  //Initialize the light.
  _tricker.setup();
  _tricker.update();
}

void loop() {
  // Wait a bit
  delay(_tricker._driftSpeed);

  if (Serial1.available())
  {
    // We received something on BT module

    // Memory pool for JSON object tree.
    StaticJsonBuffer<800> jsonBuffer;

    // Root of the object tree.
    // It's a reference to the JsonObject, the actual bytes are inside the
    // JsonBuffer with all the other nodes of the object tree.
    // Memory is freed when jsonBuffer goes out of scope.
    JsonObject& root = jsonBuffer.parse(Serial1);

    // Test if parsing succeeds.
    if (!root.success()) {
      Serial.println("parseObject() failed");
    }

    // Decode the information
    const char* commandType = root["command"];
    Serial.print("Receveid command from BT module :");
    Serial.println(commandType);

    if (strcmp(commandType, "setSmall") == 0)
    {
      Serial.print("Going to switch the small fan with value: ");
      _tricker._small_fan_speed = root["small"];
      Serial.println(_tricker._small_fan_speed);
    }
    else if (strcmp(commandType, "setBig") == 0)
    {
      Serial.print("Going to switch the big fan with value: ");
      _tricker._big_fan_speed = root["big"];
      Serial.println(_tricker._big_fan_speed);
    }
    else if (strcmp(commandType, "setPeltier") == 0)
    {
      Serial.print("Going to switch the big fan with value: ");
      _tricker._pelltier_is_on = root["peltier"];
      Serial.println(_tricker._pelltier_is_on);
    }
    else if (strcmp(commandType, "getData") == 0)
    {
      Serial.println("Entering getRgb");
      //get T and H
      int chk = DHT.read22(DHT22_PIN);
      switch (chk)
      {
        case DHTLIB_OK:
          Serial.print("OK,\t");
          break;
        case DHTLIB_ERROR_CHECKSUM:
          Serial.print("Checksum error,\t");
          break;
        case DHTLIB_ERROR_TIMEOUT:
          Serial.print("Time out error,\t");
          break;
        case DHTLIB_ERROR_CONNECT:
          Serial.print("Connect error,\t");
          break;
        case DHTLIB_ERROR_ACK_L:
          Serial.print("Ack Low error,\t");
          break;
        case DHTLIB_ERROR_ACK_H:
          Serial.print("Ack High error,\t");
          break;
        default:
          Serial.print("Unknown error,\t");
          break;
      }
      // DISPLAY DATA
      Serial.print(DHT.humidity, 1);
      Serial.print(",\t");
      Serial.print(DHT.temperature, 1);
      Serial.print(",\t");
      Serial.println();
      // Create the root of the object tree.
      //
      // It's a reference to the JsonObject, the actual bytes are inside the
      // JsonBuffer with all the other nodes of the object tree.
      // Memory is freed when jsonBuffer goes out of scope.
      JsonObject& root2 = jsonBuffer.createObject();
      // Add values in the object
      //
      // Most of the time, you can rely on the implicit casts.
      // In other case, you can do root.set<long>("time", 1351824120);
      root2["s"] = _tricker._small_fan_speed;
      root2["b"] = _tricker._big_fan_speed;
      root2["p"] = _tricker._pelltier_is_on;
      root2["t"] = DHT.temperature;
      root2["h"] = DHT.humidity;
      root2.printTo(Serial1);
      Serial1.println();
      //String aResponse = "{r:" + String(_light._color._red) + ",b:" + String(_light._color._blue) + ",g:" + String(_light._color._green) + "}";
      //Serial1.println(aResponse);
      //Serial.println(aResponse);
    }
    else
    {
      Serial.println("Unknow command received on BT");
    }
    _tricker.update();
  }

}
