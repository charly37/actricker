    package net.djynet.AcTricker.AcTricker;

    import android.support.v7.app.AppCompatActivity;
    import android.widget.Button;
    import android.widget.CompoundButton;
    import android.widget.EditText;
    import android.widget.Toast;
    import android.content.Intent;
    import android.os.Bundle;
    import android.util.Log;
    import android.bluetooth.BluetoothAdapter;
    import android.bluetooth.BluetoothDevice;
    import android.bluetooth.BluetoothSocket;

    import java.io.BufferedReader;
    import java.io.InputStreamReader;

    import android.view.Menu;
    import android.view.MenuItem;
    import android.widget.SeekBar;
    import android.view.View;
    import android.widget.SeekBar.OnSeekBarChangeListener;
    import android.os.Looper;
    import android.os.Message;
    import android.os.Handler;
    import android.widget.ToggleButton;

    import org.json.JSONObject;

    import java.util.UUID;
    import java.io.IOException;
    import java.io.InputStream;
    import java.io.OutputStream;

    public class MainActivity extends AppCompatActivity {

        // My variables
        private BluetoothSocket _blueToothSocket = null;
        // Handle the message coming from other threads
        private Handler _ThreadMsgHandler = null;
        // Could be move in the asynch class
        private OutputStream _streamOutgoing = null;
        // Red color
        private int _smallFan = 0;
        // Blue color
        private int _bigFan = 0;
        // Green color
        private int _peltier = 0;
        private boolean _isDriftingBoard = false;
        private double _temp = 0.0;
        private double _humidity = 0.0;
        private boolean _isDriftingAndroid = false;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Log.i("MyActivity", "App going on create");
            setContentView(R.layout.activity_main);

            //Now we declare the handler which will process the message from other thread
            //We do it this way since it is the only way to update the UI and have a responsive program
            _ThreadMsgHandler = new Handler(Looper.getMainLooper()) {
                //Will be called when new message arrive from other threads
                public void handleMessage(Message msg) {
                    Log.d("MyActivity", "Handler is handling a message of type : " + msg.what);
                    switch (msg.what) {
                        //Random ID .... should be the same as the one use when sending the msg from the thread
                        case 31444:
                            String aMyMoistureValue = (String) msg.obj;
                            try {
                            JSONObject obj = new JSONObject(aMyMoistureValue);
                                Log.d("MyActivity", obj.toString());
                                _smallFan = obj.optInt("s");
                                SeekBar seekBarSmall = (SeekBar) findViewById(R.id.seekBarSmall);
                                seekBarSmall.setProgress(_smallFan);
                                _bigFan = obj.optInt("b");
                                SeekBar seekBarBig = (SeekBar) findViewById(R.id.seekBarBig);
                                seekBarBig.setProgress(_bigFan);
                                _peltier = obj.optInt("p");
                                SeekBar seekBarPeltier = (SeekBar) findViewById(R.id.seekBarPeltier);
                                seekBarPeltier.setProgress(_peltier);
                                _isDriftingBoard= obj.optBoolean("d");
                                _temp= obj.optDouble("t");
                                EditText editTextTemp = (EditText) findViewById(R.id.editTextTemp);
                                editTextTemp.setText(String.valueOf(_temp));
                                _humidity= obj.optDouble("h");
                                EditText editTextHumidity = (EditText) findViewById(R.id.editTextHumidity);
                                editTextHumidity.setText(String.valueOf(_humidity));
                                if(_isDriftingAndroid != _isDriftingBoard) {
                                    ToggleButton toggle = (ToggleButton) findViewById(R.id.toggleButton);
                                    toggle.setChecked(_isDriftingBoard);
                                }
                            // For now we only log it and toast it
                            Log.i("MyActivity", "Toasting moisture value : " + aMyMoistureValue);
                            Toast.makeText(MainActivity.this, aMyMoistureValue, Toast.LENGTH_LONG).show();
                            } catch (Throwable t) {
                                Log.e("MyActivity", "Could not parse malformed JSON: \"" + aMyMoistureValue + "\"");
                            }
                            //TextView aMyTextViewHandlingResponse = (TextView) findViewById(R.id.textView);
                            //aMyTextViewHandlingResponse.setText("Moisture: " + aMyMoistureValue);
                            break;
                        //I think it should never occurs but just to be safe we log it
                        default:
                            Log.w("MyActivity", "Received an unknown message type : " + msg.what);
                    }
                };
            };

            //Seekbar handler
            SeekBar seekBarSmall = (SeekBar) findViewById(R.id.seekBarSmall);
            seekBarSmall.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
                int progressChanged = 0;

                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
                    progressChanged = progress;
                }

                public void onStartTrackingTouch(SeekBar seekBar) {
                    // TODO Auto-generated method stub
                }

                public void onStopTrackingTouch(SeekBar seekBar) {
                    Toast.makeText(MainActivity.this,"Red seek bar progress:"+progressChanged,
                            Toast.LENGTH_SHORT).show();
                    _smallFan = progressChanged;
                    String jsonString = "{command:setSmall,small:" + String.valueOf(_smallFan) + "}";
                    sendData(jsonString);

                }
            });

            ToggleButton toggle = (ToggleButton) findViewById(R.id.toggleButton);
            toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (_isDriftingBoard != _isDriftingAndroid) {
                        Log.d("MyActivity", "Background refresh change the driffting state");
                        _isDriftingAndroid = _isDriftingBoard;

                    }
                    else{
                        Log.d("MyActivity", "user change the driffting state");
                        _isDriftingAndroid=isChecked;
                        if (isChecked) {
                            String jsonString = "{command:colorDrif}";
                            sendData(jsonString);
                        } else {
                            String jsonString = "{command:colorDrif}";
                            sendData(jsonString);
                        }
                    }
                }
            });

            SeekBar seekBarBig = (SeekBar) findViewById(R.id.seekBarBig);
            seekBarBig.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
                int progressChanged = 0;

                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
                    progressChanged = progress;
                }

                public void onStartTrackingTouch(SeekBar seekBar) {
                    // TODO Auto-generated method stub
                }

                public void onStopTrackingTouch(SeekBar seekBar) {
                    Toast.makeText(MainActivity.this,"Blue seek bar progress:"+progressChanged,
                            Toast.LENGTH_SHORT).show();
                    _bigFan = progressChanged;
                    String jsonString = "{command:setBig,big:" + String.valueOf(_bigFan) + "}";
                    sendData(jsonString);

                }
            });

            SeekBar seekBarPeltier = (SeekBar) findViewById(R.id.seekBarPeltier);
            seekBarPeltier.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
                int progressChanged = 0;

                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
                    progressChanged = progress;
                }

                public void onStartTrackingTouch(SeekBar seekBar) {
                    // TODO Auto-generated method stub
                }

                public void onStopTrackingTouch(SeekBar seekBar) {
                    Toast.makeText(MainActivity.this,"Green seek bar progress:"+progressChanged,
                            Toast.LENGTH_SHORT).show();
                    _peltier = progressChanged;
                    String jsonString = "{command:setPeltier,peltier:" + String.valueOf(_peltier) + "}";
                    sendData(jsonString);

                }
            });


        }

        //protected void retrievePairedDevices()
        //{
            //Log.i("MyActivity", "I will now list the paired devices");
            //BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            //Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
            // If there are paired devices
            //if (pairedDevices.size() > 0) {

                //Fill the spinner on view with devices
                //Spinner spinner = (Spinner) findViewById(R.id.spinner);
                // Create an empty ArrayAdapter
                //ArrayAdapter <CharSequence> adapter = new ArrayAdapter <CharSequence> (this, android.R.layout.simple_spinner_item );
                // Specify the layout to use when the list of choices appears
                //adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                // Apply the adapter to the spinner
                //spinner.setAdapter(adapter);
                //Populate IT !!
                //for (BluetoothDevice device : pairedDevices) {
                //    adapter.add(device.getName() + "\n" + device.getAddress());
                //}
            //}
        //}

        protected void sendData(String message) {

            // The thread which is listening to input data from Arduino is now started
            // We can go to next step and send request to Arduino


            //Convert the String to send to byte
            byte[] msgBuffer = message.getBytes();
            Log.d("MyActivity", "About to send : " + message);

            if (_blueToothSocket != null) {

            try {
                _streamOutgoing.write(msgBuffer);
                //The next line is sending h char. I kept it as a debug test because I was worried Android will add extra char which will not be understand on Arduino side like end of line
                //_streamOutgoing.write('h');
            } catch (IOException e) {
                Log.e("MyActivity", "Error when trying to send data : " + e.getMessage());
            }
            catch(Exception e){
                Log.e("MyActivity", "Error when trying to send data : " + e.getMessage());
            }
        }
            //This is over. The Arduino response will be handle separately by another thread that we started before sending the request
        }

        protected void sendAndReceived(String message) {
            // start thread which handle input BT data before we send the request to Arduino.
            // With this design we know that we will be able to handle response
            ImplementsRunnable rc = new ImplementsRunnable();
            Thread t1 = new Thread(rc);
            t1.start();
            // The thread which is listening to input data from Arduino is now started
            // We can go to next step and send request to Arduino


            //Convert the String to send to byte
            byte[] msgBuffer = message.getBytes();
            Log.d("MyActivity", "About to send : " + message);

            if (_blueToothSocket != null) {

                try {
                    _streamOutgoing.write(msgBuffer);
                    //The next line is sending h char. I kept it as a debug test because I was worried Android will add extra char which will not be understand on Arduino side like end of line
                    //_streamOutgoing.write('h');
                } catch (IOException e) {
                    Log.e("MyActivity", "Error when trying to send data : " + e.getMessage());
                }
                catch(Exception e){
                    Log.e("MyActivity", "Error when trying to send data : " + e.getMessage());
                }
            }
            //This is over. The Arduino response will be handle separately by another thread that we started before sending the request
        }

        protected void connectMoisture()
        {
            BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            Log.i("MyActivity", "We open the connection");
            // I will use the standard SPP UUID - remember it is not link to our device on contrary of MAC address
            UUID aSppUuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
            // The Arduino BT receiver address
            String aArduinoBtAddress = "00:06:66:60:42:BE";
            try{
                Log.i("MyActivity", "Going to retrieve the device");
                BluetoothDevice aBtDevice = mBluetoothAdapter.getRemoteDevice(aArduinoBtAddress);
                Log.i("MyActivity", "Device retrieved, creating socket now");
                _blueToothSocket = aBtDevice.createRfcommSocketToServiceRecord(aSppUuid);
                Log.i("MyActivity", "Socket created");
            }catch(IOException createSocketException){
                Log.e("MyActivity", "error1508"+ createSocketException.getMessage());
                Toast.makeText(this.getParent(), "Error when trying to contact the light with Bluetooth", Toast.LENGTH_LONG).show();
                return;
            }
            // Establish the connection.  This will block until it connects.
            Log.i("MyActivity", "Will try to connect to remote device");
            try {
                _blueToothSocket.connect();
                Log.i("MyActivity", "Connection is done");
            } catch (IOException e) {
                Log.e("MyActivity", "error148"+ e.getMessage());
                Toast.makeText(this.getParent(), "Error when trying to contact the light with Bluetooth", Toast.LENGTH_LONG).show();
                try {
                    Log.i("MyActivity", "Closing socket");
                    _blueToothSocket.close();
                    Log.i("MyActivity", "Socket closed");
                } catch (IOException e2) {
                    Log.e("MyActivity", "another issue" + e2.getMessage());
                }
                return;
            }
            // Step 3 create stream to talk with BT deice
            try {
                _streamOutgoing = _blueToothSocket.getOutputStream();
            } catch (IOException e) {
                Log.e("MyActivity", "another issue" + e.getMessage());
                Toast.makeText(this.getParent(), "Error when trying to contact the light with Bluetooth", Toast.LENGTH_LONG).show();
                return;
            }
            //If we manage to arrive here we are connected.
            Button aConnectButton = (Button) findViewById(R.id.connect);
            aConnectButton.setText("Disconnect");

        }

        // Call Back method  to get the Message form other Activity
        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data)
        {
            super.onActivityResult(requestCode, resultCode, data);
            Log.i("MyActivity", "Activity response");
            // check if the request code is same as what is passed  here it is 2
            if(requestCode==643)
            {
                Log.i("MyActivity", "The BT start up request has arrived");
                //retrievePairedDevices();
            }
        }

        /** Handle the click on "associate" button to connect the phone with Arduino BT device. */
        public void myClickHandler(View v) {
            Log.i("MyActivity", "click button");
            connectMoisture();
        }

        /** Handle the click on "moisture request" button to send a message to arduino board. */
        public void randColorClick(View v) {
            Log.i("MyActivity", "Click random color button.");
            Log.d("MyActivity", "Send message to set random color.");
            String jsonString = "{command:randColor}";
            sendData(jsonString);
            Log.d("MyActivity", "Send message to retrieve the new RGB values.");
            jsonString = "{command:getData}";
            sendAndReceived(jsonString);

        }

        /** Handle the click on "ForceRefreshState" button to send a message to arduino board to have its current state. */
        public void ForceRefreshStateClick(View v) {
            Log.i("MyActivity", "click ForceRefreshStateClick button");
            String jsonString = "{command:getData}";
            sendAndReceived(jsonString);

        }

        //Created with project....not sure if used
        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.my, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();
            if (id == R.id.action_settings) {
                return true;
            }
            return super.onOptionsItemSelected(item);
        }

        //In case I need them later
        @Override
        public void onPause() {
            super.onPause();  // Always call the superclass method first
            Log.i("MyActivity", "App going on pause");

        }

        @Override
        public void onResume() {
            super.onResume();  // Always call the superclass method first
            Log.i("MyActivity", "App going on resume");
        }

        @Override
        protected void onStop() {
            super.onStop();  // Always call the superclass method first

           if (_blueToothSocket != null) {


            //We close the socket when leaving
            try {
                _blueToothSocket.close();
            } catch (IOException e) {
                Log.e("MyActivity", "another issue" + e.getMessage());
            }
        }
            Log.i("MyActivity", "App going on stop");
//R.id.action_settings
            Button aConnectButton = (Button) findViewById(R.id.connect);
            aConnectButton.setText("Connect");
        }

        protected void onStart() {
            super.onStart();  // Always call the superclass method first
            Log.i("MyActivity", "App going on start");
            // Test if we have BT radio on phone
            BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBluetoothAdapter == null) {
                // Device does not support Bluetooth
                Log.w("MyActivity", "Device does not support Bluetooth");
                Toast.makeText(this.getParent(), "Go buy a more recent phone", Toast.LENGTH_LONG).show();
            }
            else
            {
                Log.i("MyActivity", "Device does support Bluetooth");
                // Enable the BT if we have a radio
                // we do it nicely since we are not BT admin and thus use intend to request another activity
                // We thus need to defined a activity receiver too.... see function onActivityResult
                // 643 is random code I choose to identify my activity return in onActivityResult
                if (!mBluetoothAdapter.isEnabled()) {
                    Log.i("MyActivity", "BT is not enabled. I request to start it");
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBtIntent, 643);
                }
                // We use intent since we didn't request the ADMIN permission on BT otherwise we could have done
                // bluetoothAdapter.enable();
                // OK if we arrive here it means that the BT is already properly enabled so we continue
                //retrievePairedDevices();
                //connect to the device
                connectMoisture();
            }
        }


        @Override
        protected void onRestart() {
            super.onRestart();  // Always call the superclass method first
            Log.i("MyActivity", "Ap2p going on restart");
        }

        /**
         * Threadable class which run in background to receive data FROM the arduino board and forward to the UI class.
         */
        class ImplementsRunnable implements Runnable {
            private InputStream _streamIncoming = null;

            public void run() {
                Log.d("MyActivity", "Thread run");

                //First we retrieve the input stream of the BT socket to be able to read incoming bytes
                try {
                    _streamIncoming = _blueToothSocket.getInputStream();
                    Log.i("MyActivity", "Input stream acquired");
                } catch (IOException e) {
                    Log.e("MyActivity", "another issue" + e.getMessage());
                }

                //This single read works because I only wait for one message from Arduino but we should have a infinite loop here to catch everything arriving all the time
                try {
                    //Not clean but we only receive a small message
                    BufferedReader aMyBufferReader = new BufferedReader(new InputStreamReader(_streamIncoming));
                    String aArduinoDataStr = "";
                    aArduinoDataStr = aMyBufferReader.readLine();

                    Log.i("MyActivity", "Received " + aArduinoDataStr );
                    _ThreadMsgHandler.obtainMessage(31444, aArduinoDataStr).sendToTarget();     // Send to message queue Handler

                } catch (IOException e) {
                    Log.e("MyActivity", "another issue" + e.getMessage());
                }
                Log.i("MyActivity", "Leaving thread run method " );
            }
        }
    }
